/*Create Super-heros database*/

CREATE DATABASE SuperherosDb

/*Create Tables
1 Superhero
2 Assistant
3 Power  
And id as PRIMARY KEY */


USE  SuperherosDb
GO
CREATE TABLE Superhero (
    Id int NOT NULL IDENTITY(1,1) PRIMARY KEY,
    Name nvarchar(50),
    Alias nvarchar(50),
    Origin nvarchar(50),
);
CREATE TABLE Assistant (
    Id int NOT NULL IDENTITY(1,1) PRIMARY KEY,
    Name nvarchar(50),
);
CREATE TABLE Power (
    Id int NOT NULL IDENTITY(1,1) PRIMARY KEY,
    Name nvarchar(50),
	Description nvarchar(255)
);

/*ALTER Assistant table and add FOREIGN KEY*/

ALTER TABLE Assistant
ADD SuperheroId int,
FOREIGN KEY (SuperheroId) REFERENCES Superhero(Id)

/*  Superhero_Power linking table*/

CREATE TABLE Superhero_Power(SuperheroId int , PowerId int)

/* Add Superhero_Power linking table constraint */
ALTER TABLE Superhero_Power
ADD CONSTRAINT FK_SuperheroId
FOREIGN KEY (SuperheroId) REFERENCES Superhero(Id);
ALTER TABLE Superhero_Power
ADD CONSTRAINT FK_PowerId
FOREIGN KEY (PowerId) REFERENCES Power(Id);


/*Populate tables
1 Superhero with some data*/

INSERT INTO Superhero(Name, Alias, Origin)
VALUES ('Atmar','Tom B. Erichsen','Norway');
INSERT INTO Superhero(Name, Alias, Origin)
VALUES ('Idnan','Erichsen','Danmark');
INSERT INTO Superhero(Name, Alias, Origin)
VALUES ('Mikkel','Som A. Erichsen','Sweden');

/*Populate tables
1 Assistant with some data*/

INSERT INTO Assistant(Name,SuperheroId)
VALUES ('AtmarAssistant',1);
INSERT INTO Assistant(Name,SuperheroId)
VALUES ('IdnanAssistant',2);
INSERT INTO Assistant(Name,SuperheroId)
VALUES ('MikkelAssistant',3)

/*Populate tables
1 Power with some data*/

INSERT INTO Power(Name,Description)
VALUES ('AtmarSuperPower',' Power makes fire');
INSERT INTO Power(Name,Description)
VALUES ('IdnanSuperPower','Power makes water');
INSERT INTO Power(Name,Description)
VALUES ('MikkelSuperPower','Power makes Air');
INSERT INTO Power(Name,Description)
VALUES ('MikkelSuperPower','Power makes Hot');

/*UPDATE Superhero with new data*/

UPDATE Superhero
SET Name = 'AmitaBachan',Alias ='New Name',Origin='Hong'
WHERE Id = 1;


/*Try to delete from Superhero  but because of foreign key the delation faild */

DELETE FROM Superhero WHERE id=1;

/*Msg 547, Level 16, State 0, Line 1
The DELETE statement conflicted with the REFERENCE constraint "FK_SuperheroId". The conflict occurred in database "SuperherosDb", table "dbo.Superhero_Power", column 'SuperheroId'.
The statement has been terminated.

Completion time: 2021-05-21T15:33:12.9923751+02:00*/


/*Delete assistant*/
DELETE FROM Assistant WHERE Name='AtmarAssistant';